#!/usr/bin env python

from distutils.core import setup

setup(
	name = "fgbucket",
	version = "0.1",
	description = "Python interface to aid working with simgear bucket data",
	author = "Jacob Burbach",
	author_email = "jmburbach@gmail.com",
	url = "http://gitorious.org/fg-scenery-tools",
	license = "LGPLv3",
	packages = [ "fgbucket" ]
)	
