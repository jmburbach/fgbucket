# This file is part of fg-scenery-tools <https://gitorious.org/fg-scenery-tools>
#
# Copyright (C) 2011 Jacob M. Burbach <jmburbach@gmail.com>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License, version 3, as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
# Derived from the simgear SGBucket class <https://gitorious.org/fg/simgear>
###############################################################################
BUCKET_SPAN = 0.125
HALF_BUCKET_SPAN = 0.5 * BUCKET_SPAN


def bucket_span(lat):
	if  lat >= 89.0:
		return 360.0
	elif lat >= 88.0:
		return 8.0
	elif lat >= 86.0:
		return 4.0
	elif lat >= 83.0:
		return 2.0
	elif lat >= 76.0:
		return 1.0
	elif lat >= 62.0:
		return 0.5
	elif lat >= 22.0:
		return 0.25
	elif lat >= -22.0:
		return 0.125
	elif lat >= -62.0:
		return 0.25
	elif lat >= -76.0:
		return 0.5
	elif lat >= -83.0:
		return 1.0
	elif lat >= -86.0:
		return 2.0
	elif lat >= -88.0:
		return 4.0
	elif lat >= -89.0:
		return 8.0
	else:
		return 360.0


class Bucket:

	def __init__(self, index):
		lon = index >> 14
		index -= lon << 14

		lat = index >> 6
		index -= lat << 6

		y = index >> 3
		index -= y << 3

		x = index

		self._lon = lon - 180
		self._lat = lat - 90
		self._x = x
		self._y = y

	@property
	def lat(self):
		return self._lat

	@property
	def lon(self):
		return self._lon

	@property
	def x(self):
		return self._x

	@property
	def y(self):
		return self._y

	@property
	def width(self):
		if self.lon == -180 and (self.lat == -89 or self.lat == 88):
			return 4.0
		return bucket_span(self.center_lat)

	@property
	def height(self):
		return BUCKET_SPAN

	@property
	def center_lat(self):
		return self.lat + self.y / 8.0 + HALF_BUCKET_SPAN

	@property
	def center_lon(self):
		span = bucket_span(self.lat + self.y / 8.0 + HALF_BUCKET_SPAN)
		if span >= 1.0:
			return self.lon + self.width / 2.0
		else:
			return self.lon + self.x * span + self.width / 2.0

	@property
	def min_lon(self):
		return self.center_lon - 0.5 * self.width

	@property
	def min_lat(self):
		return self.center_lat - 0.5 * self.height

	@property
	def max_lon(self):
		return self.center_lon + 0.5 * self.width

	@property
	def max_lat(self):
		return self.center_lat + 0.5 * self.height


if __name__ == "__main__":
	import sys
	if len(sys.argv) < 2:
		sys.stderr.write("usage: fgbucket.py bucket_id\n")
		sys.exit(1)

	try:
		bucket_id = int(sys.argv[1])
	except:
		sys.stderr.write("bad bucket id\n")
		sys.stderr.write("usage: fgbucket.py bucket_id\n")
		sys.exit(1)

	b = Bucket(bucket_id)
	print "bucket:", bucket_id
	print "x:", b.x
	print "y:", b.y	
	print "width:", b.width
	print "height:", b.height
	print "lat:", b.lat
	print "lon:", b.lon
	print "center_lat:", b.center_lat
	print "center_lon:", b.center_lon
	print "min_lat:", b.min_lat
	print "min_lon:", b.min_lon
	print "max_lat:", b.max_lat
	print "max_lon:", b.max_lon
