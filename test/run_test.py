#!/usr/bin/env python

import sys, os, subprocess

sys.path.append("..")
from fgbucket import Bucket


if __name__ == "__main__":
	try:
		infile = sys.argv[1]
	except:
		infile = "bucket_list.txt"

	print "reading buckets from", infile

	for line in file(infile):
		bid = int(line.strip())
		b = Bucket(bid)
		
		print "testing", bid

		sg_output = subprocess.check_output(["./sg_bucket", str(bid)])
		lat, lon, center_lat, center_lon, width, height, x, y = sg_output.strip().split(",")

		assert float(lat) == b.lat
		assert float(lon) == b.lon
		assert float(center_lat) == b.center_lat
		assert float(center_lon) == b.center_lon
		assert float(width) == b.width
		assert float(height) == b.height
		assert int(x) == b.x
		assert int(y) == b.y
